package com.gatewayserver;

import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.cloud.gateway.handler.predicate.RoutePredicateFactory;
import org.springframework.web.server.ServerWebExchange;

import java.util.function.Predicate;

public class MyRoutePredicateFactory extends AbstractRoutePredicateFactory {

    public MyRoutePredicateFactory(Class configClass) {
        super(configClass);
    }
    @Override
    public Predicate<ServerWebExchange> apply(Object config) {
        return null;
    }
}
