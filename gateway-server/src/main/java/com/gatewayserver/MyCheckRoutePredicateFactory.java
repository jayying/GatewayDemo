package com.gatewayserver;

import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.cloud.gateway.handler.predicate.GatewayPredicate;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.function.Predicate;

@Component
public class MyCheckRoutePredicateFactory extends AbstractRoutePredicateFactory<MyCheckRoutePredicateFactory.Config> implements Ordered {
    public MyCheckRoutePredicateFactory() {
        super(Config.class);
    }

    @Override
    public Predicate<ServerWebExchange> apply(Config config) {
        return new GatewayPredicate() {
            @Override
            public boolean test(ServerWebExchange serverWebExchange) {
                System.out.println("执行自定义断言工厂,获取yml的config为"+config.getValue());
                return true;
            }
        };
    }

    @Override
    public int getOrder() {
        return -10;
    }

    public static class Config{
        String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
