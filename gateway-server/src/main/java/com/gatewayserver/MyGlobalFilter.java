package com.gatewayserver;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.ReactiveLoadBalancerClientFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Component
public class MyGlobalFilter implements GlobalFilter,Ordered {
    private static final Logger log = LoggerFactory.getLogger(MyGlobalFilter.class);
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("进入globalFilter");
//        if(true)
//        return exchange.getResponse().setComplete();
        /*CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://localhost:8100/order");
        try {
            JSONObject responseBody = httpClient.execute(httpGet, new ResponseHandler<JSONObject>() {
                @Override
                public JSONObject handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                    HttpEntity entity = response.getEntity();
                    String result= EntityUtils.toString(entity);
                    log.error("Jay" + result);
                    return null;
                }
            });
            log.error("Jay"+ responseBody);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return chain.filter(exchange).then(
                Mono.fromRunnable(()->{
                    System.out.println("post,globalFilter返回");
                })
        );

    }


    @Override
    public int getOrder() {
        return -300;
    }
}
