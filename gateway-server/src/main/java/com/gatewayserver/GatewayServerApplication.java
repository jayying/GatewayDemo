package com.gatewayserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;


@SpringBootApplication
@EnableDiscoveryClient
public class GatewayServerApplication {

    /**
     * 基本的转发
     * 当访问http://localhost:8080/jd
     * 转发到http://jd.com
     * @param builder
     * @return
     */
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder builder) {
        ZonedDateTime minusTime = LocalDateTime.now().minusHours(1).atZone(ZoneId.systemDefault());

        //System.out.println(predicate.test("syss"));
        return builder.routes()
                .route(r ->r.path("/1/2/3/order")
                    .filters(f ->f.filters(gatewayFilter()).stripPrefix(3))
                        .uri("http://localhost:8100")
                )
                .route(r ->r.path("/how/who/order")
                        .filters(f ->f.stripPrefix(2).addResponseHeader("X-Response-Red", "Blue").filters(gatewayFilter()))
                        .uri("http://localhost:8100"))
                //basic proxy
                //每个.route就是单个路由 -- 只有符合规则才执行
                //global 就作用于全部 --一定执行
                //.route(r ->r.path("/ha").filters(f ->f.filters(gatewayFilter())).uri("http://www.baidu.com"))//这里就是gatewayfilter
                //.route(r ->r.path("/ba/ta/ka").filters(f ->f.stripPrefix(6)).uri("http://localhost:8300"))
                .route(r ->r.path("/order/retry").filters(f ->f.retry(1)).uri("http://localhost:8100"))
                .route(r ->r.path("/tencent").filters(f ->f.addResponseHeader("MyResponse", "Jay")).uri("http://www.taobao.com"))
                //.route(r ->r.remoteAddr("127.0.0.1").uri("http://jd.com").id("jd_route"))
                .route(r ->r.path("/jd").uri("http://www.baidu.com/").id("baidu_route"))
                //.route(r ->r.after(minusTime).uri("http://www.taobao.com").id("taobao_route"))//在minusTime之后请求可以进，同理得Before，Between
                .build();
    }
    @Bean
    public GatewayFilter gatewayFilter(){
        return new MyGatewayFilter();
    }

    public static void main(String[] args) {
//        Predicate<String> predicate = s -> {
//            return s.contains("sys");
//        };
//        Predicate<String> predicate1 = s -> {
//            return s.equals("hhhsys");
//        };
//        System.out.println(predicate.negate().test("hhss"));

        SpringApplication.run(GatewayServerApplication.class, args);
    }

}
