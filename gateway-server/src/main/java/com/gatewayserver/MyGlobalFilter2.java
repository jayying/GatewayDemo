package com.gatewayserver;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class MyGlobalFilter2 implements GlobalFilter, Ordered {
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        System.out.println("进入globalFilter2");
        return chain.filter(exchange).then(
                Mono.fromRunnable(()->{
                    System.out.println("post,globalFilter2返回");
                })
        );

    }

    @Override
    public int getOrder() {
        return -200;
    }
}
