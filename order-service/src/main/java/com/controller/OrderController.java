package com.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("/order")
public class OrderController {
    private static AtomicInteger integer = new AtomicInteger(0);
    @GetMapping
    public String getOrderList(HttpServletRequest request){
        System.out.println("receive a request.");
        return "show all the orders." + request.getHeader("X-Request-blue");
    }
    @GetMapping("/retry")
    public String testTry(){
        int i = integer.incrementAndGet();
        if(i < 4){
            throw new RuntimeException("please retry.");
        }
        integer.set(0);
        return "服务请求成功！";
    }
}
