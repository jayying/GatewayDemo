package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductEntry {
    public static void main(String[] args) {
        SpringApplication.run(ProductEntry.class, args);
    }
}
