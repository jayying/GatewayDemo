import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Entry {
    public static void main(String[] args) {
        while (true) {
            try {
                String[] cmd = {"curl", "http://localhost:8848/nacos/v1/ns/instance/beat?serviceName=nodejs&beat={}"};
                ProcessBuilder p = new ProcessBuilder(cmd);
                p.start();
                System.out.println("send a heartbeat.");
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
